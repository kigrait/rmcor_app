import React, {Component} from 'react'
import Navbar from '../Navbar/Navbar'
import Footer from '../Footer/Footer'
import Imageslider from '../Imageslider/Imageslider';
import { Button, Nav, NavLink } from 'react-bootstrap'
import './Landing.css'
import {toast} from 'react-toastify'

import {
  BrowserRouter,
  Routes,
  Route,
  Link  } from "react-router-dom"

class Landing extends Component{
        constructor(props) {
            super(props);
            this.state = {
              name: '',
              email: '',
              message: '',
              captionStyle:'',
              slideNumberStyle:'',
              firstname: "",
          emailid: "",
          country: "india",
          subject:"",
          mobileNumber_val:""

            }

            this.handleInputChange = this.handleInputChange.bind(this);
            this.handleSubmit = this.handleSubmit.bind(this);
          }

          handleInputChange = (event) => {
            event.preventDefault();
            const target = event.target;
            this.setState({
              [target.name]: target.value,
            });
          }
      
          refreshPage = () => {
            setTimeout(()=>{
              window.location.reload(false);
          }, 3000);
          }
    
          handleChange = (event) => {
            var value = event.target.value;
            console.log(value, " was selected");
            this.setState({country: event.target.value});
          }
    
          handleSubmit = (event) => {
            event.preventDefault();
            console.log("enter "+event);
            console.log("firstname "+this.state.firstname);
            console.log("country "+this.state.country);
            this.contactUsApiCall()
            
          }
    
          contactUsApiCall = async ()=>{
            console.warn("hello 123")
            var firstname_val = this.state.firstname;
            var emailid_val = this.state.emailid;
            var country_val = this.state.country;
            var subject_val = this.state.subject;
            
          var myBooleanVal = this.checkIfStringHasSpecialChar(emailid_val);
          console.log("nnnnnnnnnnnnnn "+this.checkIfStringHasSpecialChar(emailid_val))
          let items = null;
          console.log("happy bbbb"+myBooleanVal)
          var registerUserId_val = null
          try{
             registerUserId_val = JSON.parse(sessionStorage.getItem('res')).data.pid;
          }catch(e){
            console.log("user can't login")
          }
          
          if (myBooleanVal){
            console.log("if")
            items = {name:firstname_val, emailId:emailid_val,country:country_val, subject:subject_val, registeredUserId:registerUserId_val};
          }else{
            console.log("else")
            items = {name:firstname_val,country:country_val, subject:subject_val, mobileNumber:emailid_val, registeredUserId:registerUserId_val};
          }
          
          console.log("value  "+JSON.stringify(items))
          let result = null
          try{
            result = await fetch("http://localhost:9100/user/rmcor-contactus",{
            method: 'POST',
            headers:{
              "Content-Type":"application/json"
            },
            body: JSON.stringify(items)
          });
          result = await result.json();
          if(result.status == "S10001"){
            toast.success('detail submitted Successfully.')
          }else{
            toast.error('Failed to submit.')
          }
          
        }catch(e){
          console.log('Error')
        }
          this.refreshPage()
          }
    
          checkIfStringHasSpecialChar = (_string) => {
            console.log("_string "+_string)
              let testEmail = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
              if(testEmail.test(_string)){
                return true;
              } else {
                return false;
              }
          }

    render(){
    return (
        <>
        <div className='vert-align1'><Navbar /></div>
        
        <div className='image-slider'><Imageslider /></div>
        {/* <div style={{ width: "100%" }}>
  <div
    style={{ width: "50%", height: 100, float: "left", backgroundImage: "https://www.w3schools.com/w3images/team3.jpg" }}
  >
    Left Div
  </div>
  <div style={{ marginLeft: "50%", height: 100, background: "blue" }}>
    Right Div
  </div>
</div> */}

<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <img src="https://i.ibb.co/MC4KhQF/software-development.jpg" style={{ marginLeft: "7%", height: 348, width: "122%" }}/>
  <p  style={{ marginLeft: "2%", height: 230, marginRight: "3%", fontSize:"21px", color:"black"}}><h1>Software Development</h1>Rmcor.Inc is one of the leading global custom software development company with customers spanned across india and working in more than 10 industry ...  <div className="login-btn123">
            <Link to="/softwareDevDetail"><Button type="primary" style={{backgroundColor:"red"}}>READ MORE</Button></Link>
            </div></p>
           
            
</div>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
<p  style={{ marginLeft: "2%", height: 230, marginRight: "3%", fontSize:"21px", color:"black"}}><h1>Mobile Apps Development</h1>Rmcor.Inc is one of the best mobile apps development services in India. Now Get Cost effective mobile Apps. We are an excellent, most trusted & reliable mobile app development services in India ... <div className="login-btn123">
            <Link to="/mobileAppDetail"><Button type="primary" style={{backgroundColor:"red"}}>READ MORE</Button></Link>
            </div></p>
              <img src="https://i.ibb.co/ydLznGL/Mobile-Application-Software.jpg" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>
</div>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <img src="https://i.ibb.co/FwLNNgF/customer-care.jpg" style={{ marginLeft: "7%", height: 348, width: "122%" }}/>
  <p  style={{ marginLeft: "2%", height: 230, marginRight: "3%", fontSize:"21px", color:"black"}}><h1>Customer Service's</h1>Rmcor.Inc is a leading provider of customer support service without lowering the quality of the work with the help of the outsourced 24/7 support. Only dedicated multilingual customer support teams available 24/7 ...  <div className="login-btn123">
            <Link to="/customerServiceDetail"><Button type="primary" style={{backgroundColor:"red"}}>READ MORE</Button></Link>
            </div></p>
            </div>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
<p  style={{ marginLeft: "2%", height: 230, marginRight: "3%", fontSize:"21px", color:"black"}}><h1>IT Training </h1>Rmcor.Inc is the technology workforce development company that helps teams know more and work better together with stronger skills, improved processes and  ...  <div className="login-btn123">
            <Link to="/iTTraningDetail"><Button type="primary" style={{backgroundColor:"red"}}>READ MORE</Button></Link>
            </div></p>
              <img src="https://i.ibb.co/wR3Xr6P/IT-training.jpg" style={{ marginLeft: "2%", height: 348, width: "51%" }}/>
</div>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <img src="https://i.ibb.co/bm4fgNc/IT-Recruiter-India-Employer-Forum.png" style={{ marginLeft: "7%", height: 348, width: "122%" }}/>
  <p  style={{ marginLeft: "7%", height: 230, marginRight: "5%", fontSize:"21px", color:"black"}}><h1>IT Recruiters</h1>Rmcor.Inc is a national staffing firm specializing in IT recruiting. We partner with leading companies to match in-demand top talent with rewarding ...  <div className="login-btn123">
            <Link to="/iTRecruiterDetail"><Button type="primary" style={{backgroundColor:"red"}}>READ MORE</Button></Link>
            </div></p>
            </div>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
<p  style={{ marginLeft: "11%", height: 230, marginRight: "5%", fontSize:"21px", color:"black"}}><h1>Logistics Service's</h1>Rmcor.Inc is top logistics company in India and one-stop supply chain management solution that provides logistics solutions across 160+ countries ...  
<div className="login-btn123-logistics">
            <Link to="/logisticsDetails"><Button type="primary" style={{backgroundColor:"red"}}>READ MORE</Button></Link>
            </div></p>
              <img src="https://i.ibb.co/vQGy6mv/Logistics-Disruption.png" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>
</div>




         <div className="container" style={{marginTop:"90px"}}>
  <div className="row">
    <div className="column">
      <img src="https://i.ibb.co/BwVwrhJ/contact-us.jpg" style={{ width: "100%" }} />
    </div>
    <h2 style={{ textAlign: "center", fontWeight:"bold", fontSize:"25px",color:"#354068f2",textDecorationLine: "underline" }}>Get in Touch with Us</h2>
    <div className="column">
    <form onSubmit={this.handleSubmit} autoComplete="off">
        <label htmlFor="fname" style={{fontWeight:"bold", fontSize:"15px" }}>Name</label>
         <input
              name="firstname"
              type="text"
              placeholder="Your Name....."
              value={this.state.firstname}
              onChange={this.handleInputChange}
            />
        <label htmlFor="email" style={{fontWeight:"bold", fontSize:"15px" }}>Email Id / Mobile No</label>
         <input
              name="emailid"
              type="text"
              placeholder="Your Email / Mobile no....."
              value={this.state.emailid}
              onChange={this.handleInputChange}
            />

       

<label htmlFor="country" style={{fontWeight:"bold", fontSize:"15px" }}>Country</label>
<select value={this.state.value} onChange={this.handleChange}>
        <option value="india">India</option>
          <option value="australia">Australia</option>
          <option value="canada">Canada</option>
          <option value="usa">USA</option>
      </select>
        <label htmlFor="subject" style={{fontWeight:"bold", fontSize:"15px" }}>Subject</label>
         <textarea
              name="subject"
              type="text"
              placeholder="Your Subject....."
              value={this.state.subject}
              onChange={this.handleInputChange}
              required="required"
            />
        <input type="submit" defaultValue="Submit" />
      </form>
    </div>
  </div>
</div>
       <div class='footer-icon'>
       <Footer />  
       </div>
      
        </>
    )
    }
}

export default Landing
