import React, {Component} from 'react'
import Navbar from '../Navbar/Navbar'
import Footer from '../Footer/Footer'
import './About.css'


class About extends Component{
        constructor(props) {
            super(props);
            this.state = {
              name: '',
              email: '',
              message: ''
            }
          }
    render(){
    return (
        <>
         <Navbar />
        <div class="about-section">
  <h1 style={{ textAlign: "center", fontWeight:"bold", fontSize:"25px",color:"white" }}>Innovation and inguentiy Always, are the distinctive hallmarks of the work we do,
</h1>
<h1 style={{ textAlign: "center", fontWeight:"bold", fontSize:"25px",color:"white" }}>
We are Rmcor!</h1>
</div>


<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"950px" }}>Why Rmcor?</h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  {/* <img src="https://i.ibb.co/MC4KhQF/software-development.jpg" style={{ marginLeft: "7%", height: 348, width: "122%" }}/> */}
  <p  style={{ fontSize:"25px",color:"black",marginRight:"102px", marginTop:"14px" }}>We are a well formed team that provides the complete range of IT services including Definition, Development, Maintenance, Support, IT Infrastructure, QA, Training, Staffing, Process Engineering and Digital Marketing. We are an Agile team with a spectra of technology skills, supported with well-defined delivery process.</p>

  <p  style={{ fontSize:"25px",color:"black",marginRight:"10", marginTop:"20px" }}>We have a reliable and efficient global delivery and engagement model that gives flexibility to our customers. We have a rich and diverse experience in solution definition leveraging SAAS model. The team is well acquainted with experience in delivering services worldwide with a count of more than 14 countries and demonstrate high culture sustainability and good people engagement skills.</p>
           
            
</div>
</div>
<div class="about-section-123123" >
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"469px" }}>A Cherished Journey that began in 2021…</h1>
<p style={{ fontSize:"25px",color:"black",marginRight:"469px", marginTop:"20px" ,background: "#f9f9f9"}}>The idea to conceive Rmcor.Inc began almost a year ago and was driven by a lack of quality custom software development teams, with the ever evolving technological advances happening in leaps and bounce with each passing year, nuances changing on daily basis.</p>
</div>
<h1 style={{ textAlign: "center", fontWeight:"bold", fontSize:"30px", color:"#0f265e" }}>Our Industry Leaders</h1>
<div class="row">
  <div class="column">
    <div class="card1">
      <img src={"https://i.postimg.cc/rsMfWR5S/sulabh.jpg"} alt="Sulabh" style={{width:"55%"}} />
      <div class="container">
        <h2 class="name" style={{ textAlign: "center", fontWeight:"bold", fontSize:"30px", color:"#0f265e" }}>Sulabh Yadav</h2>
        <p class="title" style={{ textAlign: "center",  fontSize:"17px", color:"rgb(228 8 8)" }}>Co-Founder & CEO</p>
        <p class="desc" style={{ textAlign: "center",  fontSize:"17px", color:"rgb(122 22 10)" }}>Sulabh Yadav is Co-founder of Rmcor.Inc & known for his soft-spoken, diplomatic nature. She has over 12+ years of global delivery experience in building strong relationships with multiple enterprise clients in the US, Europe and APAC.  In his previous roles he has worked with Capgemini, Marlabs and Cognizant, managing large accounts and delivering business critical services to Fortune 500 organizations.</p>
        <p class="mail">sulabh@rmcor.com</p>
        <p><button class="button" onClick={(e) => {
      e.preventDefault();
      window.open('https://www.linkedin.com/in/rmcor-offical-4399bb19a/');
      }}>Contact</button></p>
      </div>
    </div>
  </div>

  <div class="column">
    <div class="card2">
      {/* <img src={"https://i.postimg.cc/76fH4b5n/shilu.jpg"} alt="soni" style={{width:"50%"}} /> */}
      <img src={"https://www.w3schools.com/w3images/team1.jpg"} alt="soni" style={{width:"85%"}} />
      <div class="container">
        <h2 class="name" style={{ textAlign: "center", fontWeight:"bold", fontSize:"30px", color:"#0f265e" }}>Shailendra Yadav</h2>
        <p class="title" style={{ textAlign: "center",  fontSize:"17px", color:"rgb(228 8 8)" }}>Founder & CTO</p>
        <p class="desc" style={{ textAlign: "center",  fontSize:"17px", color:"rgb(122 22 10)" }}>Shailendra Yadav founded Rmcor.Inc, and has more than 7+ years of Management and Technology experience. In this role he is focused towards establishment of engineering delivery processes, solutions building strategy, customer relationship management & customer success strategies.managing large accounts and delivering business critical services to Fortune 500 organizations. </p>
        <p class="mail">shailendra@rmcor.com</p>
        <p><button class="button" onClick={(e) => {
      e.preventDefault();
      window.open('https://www.linkedin.com/in/rmcor-offical-4399bb19a/');
      }}>Contact</button></p>
      </div>
    </div>
  </div>

  

  

  
</div>
       
      <Footer />   
        </>
    )
    }
}

export default About
