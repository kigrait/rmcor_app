import { Carousel } from 'react-carousel-minimal';

function Imageslider() {
  const data = [
    {
      image: "https://i.ibb.co/YDhLzKf/qasoftware-test-engineer-2-68-1612605930.jpg",
      caption: ""
    },
    {
      image: "https://i.ibb.co/W5j4zJ6/software-development.jpg",
      caption: ""
    },
    {
      image: "https://i.ibb.co/VwrLvXw/software-development-1.jpg",
      caption: ""
    },
    {
      image: "https://i.ibb.co/T4smKh7/Are-Mobile-Apps-worthy-for-a-company-6.jpg",
      caption: ""
    },
    {
      image: "https://i.ibb.co/d5MVcbk/24x7-customer-support.png",
      caption: ""
    },
    {
      image: "https://i.ibb.co/jf08tvh/mobile-apps-ss-1920.jpg",
      caption: ""
    },
    {
      image: "https://i.ibb.co/5cGdt7f/istockphoto-1184442198-612x612.jpg",
      caption: ""
    },
    {
      image: "https://i.ibb.co/dtK62cK/shutterstock-119664772.jpg",
      caption: ""
    },
     {
       image: "https://i.ibb.co/kqm45CW/technical-support-customer-service-business-technology-internet-concept-technical-support-customer-s.jpg",
       caption: ""
     },
     {
       image: "https://i.ibb.co/Z1jyhT3/best-logistic-services.png",
       caption: ""
     },
     {
      image: "https://i.ibb.co/FKNTtGH/global-logistics.jpg",
      caption: ""
    },
     {
       image: "https://cdn.pixabay.com/photo/2016/03/12/09/33/map-1251736_1280.jpg",
       caption: ""
     },
     {
       image: "https://i.ibb.co/0m4qBMN/software7.jpg",
       caption: ""
     },
     {
       image: "https://i.ibb.co/Fsj0SmB/software4.jpg",
       caption: ""
     },
     {
       image: "https://i.ibb.co/LzXZM5z/software5.jpg",
       caption: ""
     },
     {
       image: "https://i.ibb.co/gPGMXBT/75307106-log-stica-logo.webp",
       caption: ""
     }
   ];
 
   const captionStyle = {
     fontSize: '2em',
     fontWeight: 'bold',
   }
   const slideNumberStyle = {
     fontSize: '20px',
     fontWeight: 'bold',
   }
   return (
     <div className="App">
       <div style={{ textAlign: "center" }}>
         
         <div style={{
           padding: "0 20px"
         }}>
           <Carousel
             data={data}
             time={2000}
             width="100%"
             height="350px"
             captionStyle={captionStyle}
             radius="10px"
             slideNumber={false}
             slideNumberStyle={slideNumberStyle}
             captionPosition="bottom"
             automatic={true}
             dots={false}
             pauseIconColor="white"
             pauseIconSize="40px"
             slideBackgroundColor="darkgrey"
             slideImageFit="cover"
             thumbnails={false}
             thumbnailWidth="100px"
             style={{
               textAlign: "center",
               maxWidth: "100%",
               maxHeight: "500px",
               margin: "40px auto",
             }}
           />
         </div>
       </div>
     </div>
   );
 }
 
 export default Imageslider;

