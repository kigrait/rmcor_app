import React, {Component} from 'react'
import Navbar from '../Navbar/Navbar'
import Footer from '../Footer/Footer'
import './Services.css'
import {
	BrowserRouter,
	Routes,
	Route,
	Link  } from "react-router-dom";

class Services extends Component{
    constructor(props) {
        super(props);
        this.state = {
          name: '',
          email: '',
          message: ''
        }
      }
    
      onNameChange(event) {
        this.setState({name: event.target.value})
      }
    
      onEmailChange(event) {
        this.setState({email: event.target.value})
      }
    
      onMessageChange(event) {
        this.setState({message: event.target.value})
      }
    
      handleSubmit(event) {
      }
      render() {
    return (
        <>
         <Navbar />


<body>

	<section class="section-services">
		<div class="container">
				<div class="col-md-10 col-lg-8">
					<div class="header-section">
						<h1 class="title"><span>Our</span> Exclusive <span>Services</span></h1>
					</div>
				</div>
			<div class="row">
				<div class="col-md-6 col-lg-4">
					<div class="single-service">
						<div class="part-1">
							<i class="fab fa-500px"></i>
							<h1 class="title">Software Development</h1>
						</div>
						<div class="part-2">
							<p class="description">Rmcor.Inc is one of the leading global custom software development company with customers spanned across india and working in more than 10 industry .... </p>
							{/* <a href="#"><i class="fas fa-arrow-circle-right"></i>Read More</a> */}
							<Link to="/softwareDevDetail"><i class="fas fa-arrow-circle-right"></i>Read More</Link>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-lg-4">
					<div class="single-service">
						<div class="part-1">
							<i class="fab fa-angellist"></i>
							<h1 class="title">Mobile Application Development</h1>
						</div>
						<div class="part-2">
							<p class="description">Rmcor.Inc is one of the best mobile apps development services in India. Now Get Cost effective mobile Apps. We are an excellent, most trusted & reliable mobile app development services in India ....</p>
							<Link to="/mobileAppDetail"><i class="fas fa-arrow-circle-right"></i>Read More</Link>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-lg-4">
					<div class="single-service">
						<div class="part-1">
							<i class="fas fa-award"></i>
							<h1 class="title">Customer Service's </h1>
						</div>
						<div class="part-2">
							<p class="description">Rmcor.Inc is a leading provider of customer support service without lowering the quality of the work with the help of the outsourced 24/7 support. Only dedicated multilingual customer support teams available 24/7 ....</p>
								<Link to="/customerServiceDetail"><i class="fas fa-arrow-circle-right"></i>Read More</Link>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-lg-4">
					<div class="single-service">
						<div class="part-1">
							<i class="fas fa-broadcast-tower"></i>
							<h1 class="title">IT Training</h1>
						</div>
						<div class="part-2">
							<p class="description">Rmcor.Inc is the technology workforce development company that helps teams know more and work better together with stronger skills, improved processes and ....</p>
							<Link to="/iTTraningDetail"><i class="fas fa-arrow-circle-right"></i>Read More</Link>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-lg-4">
					<div class="single-service">
						<div class="part-1">
							<i class="fas fa-broadcast-tower"></i>
							<h1 class="title">IT Recruiters</h1>
						</div>
						<div class="part-2">
							<p class="description">Rmcor.Inc is a national staffing firm specializing in IT recruiting. We partner with leading companies to match in-demand top talent with rewarding ....</p>
							<Link to="/iTRecruiterDetail"><i class="fas fa-arrow-circle-right"></i>Read More</Link>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-lg-4">
					<div class="single-service">
						<div class="part-1">
							<i class="fab fa-canadian-maple-leaf"></i>
							<h1 class="title">Logistics Service's</h1>
						</div>
						<div class="part-2">
							<p class="description">Rmcor.Inc is top logistics company in India and one-stop supply chain management solution that provides logistics solutions across 160+ countries ....</p>
							<Link to="/logisticsDetails"><i class="fas fa-arrow-circle-right"></i>Read More</Link>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</section>

</body>

      <Footer />   
        </>
    )
      }
}

export default Services
