import React, {Component} from 'react'
import Navbar from '../Navbar/Navbar'
import Footer from '../Footer/Footer'

class CustomerServiceDetail extends Component{
    constructor(props) {
        super(props);
        this.state = {
          name: '',
          email: '',
          message: ''
        }
      }
    
      onNameChange(event) {
        this.setState({name: event.target.value})
      }
    
      onEmailChange(event) {
        this.setState({email: event.target.value})
      }
    
      onMessageChange(event) {
        this.setState({message: event.target.value})
      }
    
      handleSubmit(event) {
      }
      render() {
    return (
        <>
         <Navbar />
         <>
         <div class="about-section-123123" >
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-92px" }}>Customer Service's</h1>
<p style={{ fontSize:"25px",color:"black",marginRight:"-5px", marginTop:"20px" ,background: "#f9f9f9"}}>Customer service is the support you offer your customers — both before and after they buy and use your products or services — that helps them have an easy and enjoyable experience with you.</p>
</div>
<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <img src="https://i.ibb.co/JvqPb1j/customersupport.png" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>           
</div>
</div>
         <div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <p  style={{ fontSize:"19px",color:"black",marginRight:"1px", marginTop:"21px" }}>Rmcor has wide experience in providing round-the-clock professional customer support outsourcing services to clients around the world. We tap into this experience to help you lift the quality of customer service and set your business apart from others.</p>
  <img src="https://i.ibb.co/zxM9tqn/new-blgo-600x478.jpg" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>               
</div>
</div>

<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <img src="https://i.ibb.co/d5MVcbk/24x7-customer-support.png" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>           
</div>
</div>

<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <p  style={{ fontSize:"19px",color:"black",marginRight:"1px", marginTop:"21px" }}>We are an expert in helping companies augment sales through well-crafted cross-selling and up-selling strategies that have been refined over the years. Our handpicked team of expert operators are available 24 hours a day and 7 days a week, waiting to convert even the smallest opportunity that is presented to them by utilizing these strategies.</p>  
</div>
</div>

<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <img src="https://i.ibb.co/DzCKD3x/Customer-Support-1.jpg" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>          
</div>
</div>
</>


      <Footer />   
        </>
    )
      }
}

export default CustomerServiceDetail
