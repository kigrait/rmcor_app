import React, {Component} from 'react'
import Navbar from '../Navbar/Navbar'
import Footer from '../Footer/Footer'

class ITRecruiterDetail extends Component{
    constructor(props) {
        super(props);
        this.state = {
          name: '',
          email: '',
          message: ''
        }
      }
    
      onNameChange(event) {
        this.setState({name: event.target.value})
      }
    
      onEmailChange(event) {
        this.setState({email: event.target.value})
      }
    
      onMessageChange(event) {
        this.setState({message: event.target.value})
      }
    
      handleSubmit(event) {
      }
      render() {
    return (
        <>
         <Navbar />
         <>
         <div class="about-section-123123" >
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-92px" }}>IT Recruiter</h1>
<p style={{ fontSize:"25px",color:"black",marginRight:"-5px", marginTop:"20px" ,background: "#f9f9f9"}}>Rmcor is a national staffing and recruiting firm specializing in IT recruiting. We partner with leading companies to match in-demand top talent with rewarding information technology positions.</p>
</div>
<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <img src="https://i.ibb.co/XJ8zNx0/staffing.jpg" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>           
</div>
</div>
         <div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <p  style={{ fontSize:"19px",color:"black",marginRight:"1px", marginTop:"21px" }}>IT jobs are expected to increase 22% by 2020, with demand for software developers alone expected to increase from 28% to 32%. Unfortunately, the number of skilled IT professionals doesn’t quite meet this growth in demand for top talent.</p>
  <img src="https://i.ibb.co/F4wFD5b/We-are-hiring-Recruiter.png" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>               
</div>
</div>

<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <img src="https://i.ibb.co/LRCTKFX/staffing-process-1000x300.jpg" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>           
</div>
</div>

<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <p  style={{ fontSize:"19px",color:"black",marginRight:"1px", marginTop:"21px" }}>By partnering with tech recruitment companies or IT temp agencies like Sparks Group, companies can expand their network of available candidates for full-time, contract to hire, temporary staffing, and consultant roles. To actualize the top benefits of an IT staffing agency, our IT recruiters streamline and manage the entire hiring process to support your business success.</p>  
</div>
</div>

<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <img src="https://i.ibb.co/nLbVWzQ/itstaffing.jpg" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>          
</div>
</div>
</>


      <Footer />   
        </>
    )
      }
}

export default ITRecruiterDetail
