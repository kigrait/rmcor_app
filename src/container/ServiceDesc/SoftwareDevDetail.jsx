import React, {Component} from 'react'
import Navbar from '../Navbar/Navbar'
import Footer from '../Footer/Footer'

class SoftwareDevDetail extends Component{
    constructor(props) {
        super(props);
        this.state = {
          name: '',
          email: '',
          message: ''
        }
      }
    
      onNameChange(event) {
        this.setState({name: event.target.value})
      }
    
      onEmailChange(event) {
        this.setState({email: event.target.value})
      }
    
      onMessageChange(event) {
        this.setState({message: event.target.value})
      }
    
      handleSubmit(event) {
      }
      render() {
    return (
        <>
         <Navbar />
         <>
         <div class="about-section-123123" >
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-92px" }}>Software Development…</h1>
<p style={{ fontSize:"25px",color:"black",marginRight:"-5px", marginTop:"20px" ,background: "#f9f9f9"}}>Software development is the process of creating a piece of software designed to perform a particular task. </p>
</div>
<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <img src="https://i.ibb.co/W5j4zJ6/software-development.jpg" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>           
</div>
</div>
         <div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <p  style={{ fontSize:"19px",color:"black",marginRight:"1px", marginTop:"21px" }}>We are a well formed team that provides the complete Software Development services like Web Development, Custom Software Development, Cloud Computing,  Definition & Development. We are an Agile team with a spectra of technology skills, supported with well-defined delivery process.</p>
  <img src="https://i.ibb.co/PzJzLDB/software-development-lifecycle-1024x682.jpg" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>               
</div>
</div>

<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <img src="https://i.ibb.co/5BzkXwM/industry-3087393-480.jpg" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>           
</div>
</div>

<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <p  style={{ fontSize:"19px",color:"black",marginRight:"1px", marginTop:"21px" }}>Software developers need to use AI to streamline processes, reduce waste, and hand over repetitive manual processes to a computer that can do it faster and better. A machine learning-backed hyperautomation platform will also automatically verify deployments, saving even more time. AI can help to code too, increasing speed and accuracy.</p>  
</div>
</div>

<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <img src="https://i.ibb.co/g3TKFB7/AI.webp" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>          
</div>
</div>
</>


      <Footer />   
        </>
    )
      }
}

export default SoftwareDevDetail
