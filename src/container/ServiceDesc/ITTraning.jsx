import React, {Component} from 'react'
import Navbar from '../Navbar/Navbar'
import Footer from '../Footer/Footer'

class ITTraning extends Component{
    constructor(props) {
        super(props);
        this.state = {
          name: '',
          email: '',
          message: ''
        }
      }
    
      onNameChange(event) {
        this.setState({name: event.target.value})
      }
    
      onEmailChange(event) {
        this.setState({email: event.target.value})
      }
    
      onMessageChange(event) {
        this.setState({message: event.target.value})
      }
    
      handleSubmit(event) {
      }
      render() {
    return (
        <>
         <Navbar />
         <>
         <div class="about-section-123123" >
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-92px" }}>IT Training</h1>
<p style={{ fontSize:"25px",color:"black",marginRight:"-5px", marginTop:"20px" ,background: "#f9f9f9"}}>Get access to the international-standard and world class training facilities with hands-on experience from an expert team of corporate trainers. </p>
</div>
<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <img src="https://i.ibb.co/V28HcGX/IT-Training-for-Skill-Enhancement-1.jpg" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>           
</div>
</div>
         <div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <p  style={{ fontSize:"19px",color:"black",marginRight:"1px", marginTop:"21px" }}>Build skills, motivate and retain staff, protect technical investment, develop careers, attract high caliber personnel, enable a culture of continuous improvement and learning. Whatever your reason for training, The IT Training Company is able to work with you to provide high quality technical training from some of the world’s biggest IT vendors.</p>
  <img src="https://i.ibb.co/yYGTdCb/corporate-training.jpg" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>               
</div>
</div>

<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <img src="https://i.ibb.co/9pZ7LjD/HiRes.jpg" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>           
</div>
</div>

<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <p  style={{ fontSize:"19px",color:"black",marginRight:"1px", marginTop:"21px" }}>Working closely with a range of technical partners, we use vendor certified trainers to ensure the quality and integrity of the learning experience, protecting your investment and delivering the outcomes you require.</p>  
</div>
</div>

<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <img src="https://i.ibb.co/wCcz6x3/IT-Training-croma-campus.jpg" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>          
</div>
</div>
</>


      <Footer />   
        </>
    )
      }
}

export default ITTraning
