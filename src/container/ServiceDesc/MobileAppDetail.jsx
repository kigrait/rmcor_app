import React, {Component} from 'react'
import Navbar from '../Navbar/Navbar'
import Footer from '../Footer/Footer'

class MobileAppDetail extends Component{
    constructor(props) {
        super(props);
        this.state = {
          name: '',
          email: '',
          message: ''
        }
      }
    
      onNameChange(event) {
        this.setState({name: event.target.value})
      }
    
      onEmailChange(event) {
        this.setState({email: event.target.value})
      }
    
      onMessageChange(event) {
        this.setState({message: event.target.value})
      }
    
      handleSubmit(event) {
      }
      render() {
    return (
        <>
         <Navbar />
         <>
         <div class="about-section-123123" >
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-92px" }}>Mobile Application Development…</h1>
<p style={{ fontSize:"25px",color:"black",marginRight:"-5px", marginTop:"20px" ,background: "#f9f9f9"}}>Mobile app development is the act or process by which a mobile app is developed for mobile devices, such as personal digital assistants. </p>
</div>
<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <img src="https://i.ibb.co/T4smKh7/Are-Mobile-Apps-worthy-for-a-company-6.jpg" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>           
</div>
</div>
         <div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <p  style={{ fontSize:"19px",color:"black",marginRight:"1px", marginTop:"21px" }}>We are a leading mobile app development agency known for developing custom mobile apps for iOS, Android & Hybrid (React Native, Flutter & Ionic) platforms. Our mobile app developers have the expertise to create mobile applications using AI and ML technologies.</p>
  <img src="https://i.ibb.co/9wxsTd2/UPCOMING-MOBILE-APP-DEVELOPMENT-TRENDS-IN-2021-1.jpg" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>               
</div>
</div>

<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <img src="https://i.ibb.co/89tzyMv/images.jpg" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>           
</div>
</div>

<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <p  style={{ fontSize:"19px",color:"black",marginRight:"1px", marginTop:"21px" }}>Apps we build showcase only a portion of what our mobile app development company is capable of. We know what it takes to convert your vision into reality. Having worked on numerous projects, we understand every app project is different and needs special attention. Thus, we spend a considerable time in planning and research.</p>  
</div>
</div>

<div class="about-section-123123">
<h1 style={{ fontWeight:"bold", fontSize:"35px",color:"#0f265e",marginRight:"-38px" }}></h1>
<div className="image-txt-container" style={{background: "#f9f9f9" }}>
  <img src="https://i.ibb.co/TqpyHFM/app.jpg" style={{ marginLeft: "2%", height: 348, width: "122%" }}/>          
</div>
</div>
</>


      <Footer />   
        </>
    )
      }
}

export default MobileAppDetail
