import React, {Component} from 'react'
import Navbar from '../Navbar/Navbar'
import Footer from '../Footer/Footer'
import './CommingSoon.css'

class CommingSoon extends Component{
    constructor(props) {
        super(props);
        this.state = {
          name: '',
          email: '',
          message: ''
        }
      }
    
      onNameChange(event) {
        this.setState({name: event.target.value})
      }
    
      onEmailChange(event) {
        this.setState({email: event.target.value})
      }
    
      onMessageChange(event) {
        this.setState({message: event.target.value})
      }
    
      handleSubmit(event) {
      }
      render() {
    return (
        <>
         <Navbar />
         <div className="bgimg">
         <figure>
      <img src="https://i.ibb.co/2kfsKYV/coming-g655114afe-1920.jpg" alt="" style={{ width: "100%" }} />
    </figure>
  {/* <div className="topleft">
    <p>Logo</p>
  </div>
  <div className="middle">
    <h1>COMING SOON</h1>
    <hr />
    <p>35 days</p>
  </div>
  <div className="bottomleft">
    <p>Some text</p>
  </div> */}
</div>



      <Footer />   
        </>
    )
      }
}

export default CommingSoon
