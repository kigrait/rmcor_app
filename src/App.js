import logo from './logo.svg';
import './App.css';
import Login from "./container/Login";
import Registration from './container/Registration/Registration';
import Navbar from './container/Navbar/Navbar'
import Footer from './container/Footer/Footer'
import About from './container/About/About';
import ContactUs from './container/ContactUs/ContactUs';
import Landing from './container/Landing/Landing';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Services from './container/Services/Services';
import StudentRegistartion from './container/StudentRegistartion/StudentRegistartion';
import CommingSoon from './container/CommingSoon/CommingSoon';
import {ToastContainer} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { Header } from 'antd/lib/layout/layout';
import Imageslider from './container/Imageslider/Imageslider';
import LogisticsDetails from './container/ServiceDesc/LogisticsDetails';
import Profile from './container/Profile/Profile';
import SoftwareDevDetail from './container/ServiceDesc/SoftwareDevDetail';
import MobileAppDetail from './container/ServiceDesc/MobileAppDetail';
import CustomerServiceDetail from './container/ServiceDesc/CustomerServiceDetail';
import ITTraning from './container/ServiceDesc/ITTraning';
import ITRecruiterDetail from './container/ServiceDesc/ITRecruiterDetail';

function App() {
  return (
    <div className="App">

    <Router>
        <Routes>
          <Route path="/" element={<Landing />}></Route>
          <Route path="/about" element={<About />}></Route>
          <Route path="/contact" element={<ContactUs />}></Route>
          <Route path="/signup" element={<Registration />}></Route>
          <Route path="/login" element={<Profile />}></Route>
          <Route path="/services" element={<Services />}></Route>
          <Route path="/commingSoon" element={<CommingSoon />}></Route>
          <Route path="/logisticsDetails" element={<LogisticsDetails />}></Route>
          <Route path="/Landing" element={<Landing />}></Route>
          <Route path="/imageslider" element={<Imageslider />}></Route>
          <Route path="/softwareDevDetail" element={<SoftwareDevDetail />}></Route>
          <Route path="/mobileAppDetail" element={<MobileAppDetail />}></Route>
          <Route path="/customerServiceDetail" element={<CustomerServiceDetail />}></Route>
          <Route path="/iTTraningDetail" element={<ITTraning />}></Route>
          <Route path="/iTRecruiterDetail" element={<ITRecruiterDetail />}></Route>
        </Routes>
      </Router>
     
     
<div className='container-fluid'>
<Header />
<ToastContainer autoClose={3000} />
</div>
    </div>
  );
}

export default App;
